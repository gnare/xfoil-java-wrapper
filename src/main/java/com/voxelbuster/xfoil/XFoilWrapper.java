/*
 * Copyright (C) 2023  Galen Nare
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.voxelbuster.xfoil;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.SystemUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * The {@code XFoilWrapper} class serves as a wrapper for the x86/amd64 XFoil application,
 * facilitating the downloading, installation, and execution of XFoil processes.
 */
@Slf4j
public class XFoilWrapper {

    private static final Properties downloadURLs;

    private boolean haveXfoil = Paths.get("lib", "xfoil").toFile().exists() ||
        Paths.get("lib", "xfoil.exe").toFile().exists();

    private final ProcessBuilder pb = new ProcessBuilder("./lib/xfoil");

    static {
        try {
            downloadURLs = new Properties();
            downloadURLs.load(XFoilWrapper.class.getResourceAsStream("/urls.properties"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Constructs a new {@code XFoilWrapper} and initiates the download and
     * installation of XFoil if it is not already present.
     */
    public XFoilWrapper() {
        if (!haveXfoil) {
            log.info("Installing xfoil...");
            if (!SystemUtils.OS_ARCH.equalsIgnoreCase("amd64")) {
                throw new RuntimeException("Unsupported architecture. Please run on amd64");
            }

            File archiveFile = new File("xfoil.zip");

            try {
                if (SystemUtils.IS_OS_WINDOWS) {
                    FileUtils.copyURLToFile(new URL(downloadURLs.getProperty("win")), archiveFile);
                } else if (SystemUtils.IS_OS_LINUX) {
                    FileUtils.copyURLToFile(new URL(downloadURLs.getProperty("linux")), archiveFile);
                } else if (SystemUtils.IS_OS_MAC) {
                    FileUtils.copyURLToFile(new URL(downloadURLs.getProperty("macos")), archiveFile);
                } else {
                    throw new RuntimeException("Unknown operating system");
                }

                File destDir = new File(System.getProperty("user.dir"), "lib");
                //noinspection ResultOfMethodCallIgnored
                destDir.mkdirs();

                try (ZipInputStream zis = new ZipInputStream(Files.newInputStream(Paths.get("xfoil.zip")))) {
                    byte[] buffer = new byte[1024];
                    ZipEntry zipEntry = zis.getNextEntry();
                    while (zipEntry != null) {
                        File newFile = new File(destDir, String.valueOf(zipEntry));
                        if (zipEntry.isDirectory()) {
                            if (!newFile.isDirectory() && !newFile.mkdirs()) {
                                throw new IOException("Failed to create directory " + newFile);
                            }
                        } else {
                            // fix for Windows-created archives
                            File parent = newFile.getParentFile();
                            if (!parent.isDirectory() && !parent.mkdirs()) {
                                throw new IOException("Failed to create directory " + parent);
                            }

                            // write file content
                            FileOutputStream fos = new FileOutputStream(newFile);
                            int len;
                            while ((len = zis.read(buffer)) > 0) {
                                fos.write(buffer, 0, len);
                            }
                            fos.close();
                        }
                        zipEntry = zis.getNextEntry();
                    }

                    zis.closeEntry();
                }
            } catch (IOException e) {
                log.error("Failed to download platform xFoil: ", e);
                throw new RuntimeException(e);
            }

            //noinspection ResultOfMethodCallIgnored
            archiveFile.delete();
            log.info("Done.");
        }


    }

    /**
     * Executes the XFoil process with the given standard input and logs the output.<br />
     * Note that output will not be sent back until the process completes.
     *
     * @param stdin The standard input string to be fed to the XFoil process.
     * @return The {@code Process} object corresponding to the XFoil process.
     * @throws IOException if an I/O error with the process occurs.
     * @throws InterruptedException if the process thread is interrupted while waiting.
     */
    public Process exec(String stdin) throws IOException, InterruptedException {
        Process proc = pb.start();

        OutputStreamWriter osw = new OutputStreamWriter(proc.getOutputStream());
        osw.append(stdin);
        osw.close();

        log.info("xfoil returned {}", proc.waitFor());

        log.error(IOUtils.toString(proc.getErrorStream(), StandardCharsets.UTF_8));
        log.info(IOUtils.toString(proc.getInputStream(), StandardCharsets.UTF_8));

        return proc;
    }

    /**
     * Executes the XFoil process with the given standard input silently without logging.
     *
     * @param stdin The standard input string to be fed to the XFoil process.
     * @return The {@code Process} object corresponding to the XFoil process.
     * @throws IOException if an I/O error with the process occurs.
     * @throws InterruptedException if the process thread is interrupted while waiting.
     */
    public Process execSilent(String stdin) throws IOException {
        Process proc = pb.start();

        OutputStreamWriter osw = new OutputStreamWriter(proc.getOutputStream());
        osw.append(stdin);
        osw.close();

        return proc;
    }
}

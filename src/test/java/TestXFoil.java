/*
 * Copyright (C) 2023  Galen Nare
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import com.voxelbuster.xfoil.XFoilWrapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestXFoil {

    @Test
    public void testWithStdout() throws IOException, InterruptedException {
        new XFoilWrapper().exec("HELP\nQUIT\n").waitFor();
    }

    @Test
    public void testSilent() throws IOException, InterruptedException {
        assertEquals(0, new XFoilWrapper().execSilent("HELP\nQUIT\n").waitFor());
    }
}
